<?php
/**
 * Utilisations de pipelines par Historique connexions
 *
 * @plugin     Historique connexions
 * @copyright  2020
 * @author     RealET
 * @licence    GNU/GPL
 * @package    SPIP\Histoconnexions\Pipelines
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

function histoconnexions_affiche_droite($flux) {
	if (!in_array($flux['args']['exec'], array('auteurs', 'visiteurs'))) return $flux;
	
	include_spip('inc/autoriser');
	if (!autoriser('configurer', 'plugins')) return $flux;

	include_spip('inc/presentation');	
	include_spip('public/assembler');

	if ($flux['args']['exec'] === 'auteurs') {
		$webmestres = recuperer_fond('fonds/webmestres_site');
		$webmestres = cadre_depliable(find_in_path('prive/themes/spip/images/histoconnexions-24.png'), _T('histoconnexions:webmestres'), true, $webmestres, 'bp_infos_webmasters', 'info');
		$flux['data'] .= $webmestres;
	}
	if (test_plugin_actif('date_connexion')) {
		$connectes = recuperer_fond('fonds/derniers_date_connexion', array('ecrire' => ($flux['args']['exec'] === 'auteurs' ? true : false)), array('ajax' => true));
	} else {
		$connectes = recuperer_fond('fonds/derniers_connectes', array('ecrire' => ($flux['args']['exec'] === 'auteurs' ? true : false)), array('ajax' => true));
	}
	
	$connectes .= recuperer_fond('fonds/non_confirmes');
	
	$connectes = cadre_depliable(find_in_path('prive/themes/spip/images/histoconnexions-24.png'), _T('histoconnexions:connections'), true, $connectes, 'bp_infos_connection', 'info');
	$flux['data'] .= $connectes;

	return $flux;
}