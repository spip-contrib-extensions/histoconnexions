<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'attente_confirmation' => 'En attente de confirmation',

	// C
	'connections' => 'Connexions',

	// D
	'derniers_connectes' => 'Derniers connectés :',

	// H
	'histoconnexions_titre' => 'Historique connexions',
	
	// N
	'non_confirmes' => 'Non confirmés :',

	// V
	'variable_vide' => '(Vide)',
	// W
	'webmestres' => 'Webmestres SPIP'
);
