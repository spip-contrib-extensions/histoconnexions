<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// H
	'histoconnexions_description' => 'Des cadres dépliables sur la page des membres permettant d’afficher :
-* la liste des {{Webmestres SPIP}}
-* {{Connexions}} : les derniers connectés et les inscriptions non confirmées.

Seuls les admins voient ces informations.',
	'histoconnexions_nom' => 'Historique connexions',
	'histoconnexions_slogan' => 'Suivre les dernières connexions à l’administration de SPIP',
);
