# histoconnexions

Suivre les connexions à l'administration de SPIP

Des cadres dépliables sur la page des membres permettant d’afficher :

* la liste des Webmestres SPIP
* Connexions : les derniers connectés et les inscriptions non confirmées.

Seuls les admins voient ces informations.